# deltapersistence

Package implementing the sublevelset persistent homology code for the paper

> [1] Joshua Mirth, Yanqin Zhai, Johnathan Bush, Enrique Alvarado, Howie Jordan, Mark Heim, Bala Krishnamoorthy, Markus Pflaum, Aurora Clark, Y Z, and Henry Adams, "Representations of Energy Landscapes by Sublevelset Persistent Homology: An Example With n-Alkanes," 2020, [arXiv:2011.00918](https://arxiv.org/abs/2011.00918).


## Overview

All computations from [1] can be replicated by calling the script `persistence_script.py` and this can be easily used or extended for future experiments.
Additional features are contained in three modules:
* Alkane Energy - provides methods for computing the energy landscapes of the alkane series.
* Sublevel Persistence - provides general methods for computing sublevel set persistent homology (using GUDHI).
* Data Utilities - provides a number of functions for converting data into formats GUDHI understands and manipulating GUDHI output for analysis.

For full documentation, see https://jrmirth.gitlab.io/deltapersistence/ .
Usage details for `persistence_script.py` can also be found by calling `python persistence_script.py --help` at the command line.


## Installation

Numpy, Matplotlib, and GUDHI are required dependencies.
(Details of GUDHI can be found at https://gudhi.inria.fr/.
The best installation method is probably via the [conda package](https://anaconda.org/conda-forge/gudhi).)
Verify that these three packages are installed, then clone this repository.
This code can then either be run from within the downloaded folder, or installed via pip by navigating to the cloned folder and running `pip install .`.


## Getting started

* Download and install GUDHI.
* Clone this repository.
* Navigate into the folder `deltapersistence/deltapersistence/`.
* At the command prompt, run `python3 persistence_script.py --carbons 5 --disp`.
  This should produce the barcodes for pentane.
* Run `python3 persistence_script.py -f ../tests/mesh.npy -t mesh --diagram`.
  This should generate the persistence diagram for hexane from the
  energy landscape saved as `mesh.npy`. It will take a few moments.
* Run `python3 persistence_script.py --help` to see all available
  options, then experiment with some of them!
  
## Replicating Experiments

The data corresponding to all figures in [1] are contained in the subfolder `data`.
The README file there provides details on how to replicate all experiments.

