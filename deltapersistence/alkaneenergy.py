"""Methods for computing and working with alkane energy landscapes.

The potential energy of the n-alkane molecules admits an approximate
analytical formula. This module computes that potential energy directly
at a given point or over a mesh. It also implements a more sophisticated
analytical formula for the cases of butane and pentane. Several other
utilities for working with alkane molecule names are provided.


    Copyright (C) 2020 Joshua Mirth

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


Functions
---------

* alkane_to_carbons
* carbons_to_alkane
* n_alkane_energy_mesh
* nonbonded_potential
* potential

"""

import numpy as np

# Set constants and rename things for simplicity.
# Numpy operations know to act component-wise on arrays.
PI = np.pi
cos = np.cos
sin = np.sin
sqrt = np.sqrt

def n_alkane_energy_mesh(d, res=.1, nonbonded=False):
    """Mesh of n-alkane potential energy values.

    Meshes the cube [0,2pi]^n and computes values of the potential
    energy of the alkane on that mesh.

    Parameters
    ----------
    d : int
        Dimension of the array (d = number of carbons - 3).
    res : float, optional
        Resolution at which to mesh the grid. Default is 0.1, which is
        reasonable for low-dimensional examples.
    nonbonded : bool, optional
        If true, use the more detailed formula which accounts for non-
        bonded potentials. Default is false. Currently true is only
        available if n=2 or n=3.

    Returns
    -------
    V : numpy ndarray
        Mesh of potential energy function values.

    """

    mesh = d*(np.arange(0,2*PI,res),)
    phi = np.array(np.meshgrid(*mesh, indexing='xy'))
    if nonbonded:
        V = nonbonded_potential(phi)
    else:
        V = potential_ua(phi)
    return V

def potential_aa(phi):
    """Analytical formula for the OPLS-aa n-alkane potential energy.

    Formula derived by members of YZ's group. This does not account for
    the energy between non-bonded carbon atoms.

    Parameters
    ----------
    phi : ndarray
        Mesh of the domain (n copies of [0,pi]).

    Returns
    -------
    V : numpy ndarray
        Function values over the domain.

    """

    V = ((2.9288-1.4644*cos(phi-PI)
        + 0.2092*cos(phi-PI)**2-1.6736*cos(phi-PI)**3)
        + 0.6276+1.8828*cos(phi-PI)-2.5104*cos(phi-PI)**3
        + 3*(0.6276+1.8828*cos(-phi-(1/3)*PI)-2.5104*cos(-phi-(1/3)*PI)**3)
        + 3*(0.6276+1.8828*cos(phi-(1/3)*PI)-2.5104*cos(phi-(1/3)*PI)**3))
    V = sum(V[:])      # V = V(phi_1) + V(phi_2) + ...
    return V


def potential_ua(phi):
    """Analytical formula for the OPLS-ua n-alkane potential energy.

    Formula derived by members of YZ's group. This does not account for
    the energy between non-bonded carbon atoms.

    Parameters
    ----------
    phi : ndarray
        Mesh of the domain (n copies of [0,pi]).

    Returns
    -------
    V : numpy ndarray
        Function values over the domain.

    """

    kB = 0.008314463    # Boltzmann constant in kJ/(mol*K)
    c_1 = 355.03*kB
    c_2 = -68.19*kB
    c_3 = 791.32*kB
    V = (c_1*(1+cos(phi))+c_2*(1-cos(2*phi))+c_3*(1+cos(3*phi)))
    V = sum(V[:])      # V = V(phi_1) + V(phi_2) + ...
    return V

def nonbonded_potential(phi):
    """Detailed analytical formula for n-alkane potential energy.

    Formula derived by members of YZ's group. This version accounts for
    interactions between non-bonded atoms. Currently only known for
    dimensions 2 and 3 (pentane and hexane).

    Parameters
    ----------
    phi : numpy ndarray
        Mesh of the domain (n copies of [0,pi]).

    Returns
    -------
    V : numpy ndarray
        Function values over the domain.

    Raises
    ------
    ValueError
        If the dimension of the input domain array is not 2 or 3.

    """

    # Physical constants.
    c1 = 2.9519
    c2 = -0.5670
    c3 = 6.5794
    r0 = 0.1529
    eps = 0.7322
    sig = 0.3905
    n = len(phi)
    if n == 2:  # Pentane formula.
        r15 = ((4*sqrt(3)/9)*r0*sqrt(11-4*(cos(phi[0])+cos(phi[1]))
                - 2*cos(phi[0]+phi[1])+cos(phi[0]-phi[1])))
        Vd = (c1*(1+cos(phi[0]))+c2*(1-cos(2*phi[0]))+c3*(1+cos(3*phi[0]))
              + c1*(1+cos(phi[1]))+c2*(1-cos(2*phi[1]))+c3*(1+cos(3*phi[1])))
        Vnb = (4*eps*((sig/r15)**12 - (sig/r15)**6))
        V = Vd + Vnb
        return V
    elif n == 3:    # Hexane formula.
        Vd = (c1*(1+cos(phi[0]))+c2*(1-cos(2*phi[0]))+c3*(1+cos(3*phi[0]))
              + c1*(1+cos(phi[1]))+c2*(1-cos(2*phi[1]))+c3*(1+cos(3*phi[1]))
              + c1*(1+cos(phi[2]))+c2*(1-cos(2*phi[2]))+c3*(1+cos(3*phi[2])))
        d21 = (sqrt((48/81)*r0*r0*(11-4*(cos(phi[0])+cos(phi[1]))
               - 2*cos(phi[0]+phi[1])+cos(phi[0]-phi[1]))))
        d22 = (sqrt((48/81)*r0*r0*(11-4*(cos(phi[1])+cos(phi[2]))
               - 2*cos(phi[1]+phi[2])+cos(phi[1]-phi[2]))))
        d3 = (sqrt((1/81)*r0*r0*(689-208*(cos(phi[0])+cos(phi[2]))
               - 256*cos(phi[1])+192*sin(phi[1])*(sin(phi[0])+sin(phi[2]))
               - 64*cos(phi[1])*(cos(phi[0])+cos(phi[2]))
               + 128*cos(phi[0])*cos(phi[2])
               + 48*sin(phi[1])*(cos(phi[0])*sin(phi[2])
               - sin(phi[0])*cos(phi[2]))
               + 16*cos(phi[1])*(9*sin(phi[0])*sin(phi[2])
               - cos(phi[0])*cos(phi[2])))))
        Vnb = (4*eps*((sig/d21)**12-(sig/d21)**6)+4*eps*((sig/d22)**12 
               - (sig/d22)**6)+4*eps*((sig/d3)**12-(sig/d3)**6))
        V = Vd + Vnb;
        return V
    else:
        raise ValueError('For non-bonded energy, n must be 2 or 3 (pentane or '
                         'hexane).\n The value of n was: {}'.format(n))

def alkane_to_carbons(alkane):
    """Convert an alkane name to the number of carbons it contains.

    Parameters
    ----------
    alkane : string
        Name of an n-alkane molecule

    Returns
    -------
    carbon : int
        Number of carbons in the n-alkane

    """

    carbon_dict = {
        'methane': 1, 
        'ethane': 2, 
        'propane': 3, 
        'butane': 4, 
        'pentane': 5, 
        'hexane': 6, 
        'heptane': 7, 
        'octane': 8, 
        'nonane': 9, 
        'decane': 10,
        'undecane': 11,
        'dodecane': 12,
        'tridecane': 13,
        'tetradecane': 14,
        'pentadecane': 15,
        'hexadecane': 16,
        'heptadecane': 17,
        'octadecane': 18,
        'nonadecane': 19,
        'icosane': 20,
        'henicosane': 21,
        'docosane': 22,
        'tricosane': 23,
        'tetracosane': 24,
        'pentacosane': 25,
        'hexacosane': 26,
        'heptacosane': 27,
        'octacosane': 28,
        'nonacosane': 29,
        'triacontane': 30,
        'hentriacontane': 31,
        'dotriacontane': 32,
        'tritriacontane': 33,
        'tetratriacontane': 34,
        'pentatriacontane': 35,
        'hexatriacontane': 36,
        'heptatriacontane': 37,
        'octatriacontane': 38,
        'nonatriacontane': 39,
        'tetracontane': 40,
        'hentetracontane': 41,
        'dotetracontane': 42,
        'tritetracontane': 43,
        'tetratetracontane': 44,
        'pentatetracontane': 45,
        'hexatetracontane': 46,
        'heptatetracontane': 47,
        'octatetracontane': 48,
        'nonatetracontane': 49,
        'pentacontane': 50,
        'henpentacontane': 51,
        'dopentacontane': 52,
        'tripentacontane': 53,
        'tetrapentacontane': 54,
        'pentapentacontane': 55,
        'hexapentacontane': 56,
        'heptapentacontane': 57,
        'octapentacontane': 58,
        'nonapentacontane': 59,
        'hexacontane': 60, 	
        'henhexacontane': 61, 	
        'dohexacontane': 62, 	
        'trihexacontane': 63, 	
        'tetrahexacontane': 64, 	
        'pentahexacontane': 65, 	
        'hexahexacontane': 66, 	
        'heptahexacontane': 67, 	
        'octahexacontane': 68, 	
        'nonahexacontane': 69, 	
        'heptacontane': 70, 	
        'henheptacontane': 71, 	
        'doheptacontane': 72, 	
        'triheptacontane': 73, 	
        'tetraheptacontane': 74, 	
        'pentaheptacontane': 75, 	
        'hexaheptacontane': 76, 	
        'heptaheptacontane': 77, 	
        'octaheptacontane': 78, 	
        'nonaheptacontane': 79, 	
        'octacontane': 80, 	
        'henoctacontane': 81, 	
        'dooctacontane': 82, 	
        'trioctaane': 83, 	
        'tetraoctacontane': 84, 	
        'pentaoctacontane': 85, 	
        'hexaoctacontane': 86, 	
        'heptaoctacontane': 87, 	
        'octaoctacontane': 88, 	
        'nonaoctacontane': 89, 	
        'nonacontane': 90, 	
        'hennonacontane': 91, 	
        'dononacontane': 92, 	
        'trinonacontane': 93, 	
        'tetranonacontane': 94, 	
        'pentanonacontane': 95, 	
        'hexanonacontane': 96, 	
        'heptanonacontane': 97, 	
        'octanonacontane': 98, 	
        'nonanonacontane': 99, 	
        'hectane': 100, 	
        'henihectane': 101, 	
        'dohectane': 102, 	
        'trihectane': 103, 	
        'tetrahectane': 104, 	
        'pentahectane': 105, 	
        'hexahectane': 106, 	
        'heptahectane': 107, 	
        'octahectane': 108, 	
        'nonahectane': 109, 	
        'decahectane': 110, 	
        'undecahectane': 111, 	
        'dodecahectane': 112, 	
        'tridecahectane': 113, 	
        'tetradecahectane': 114, 	
        'pentadecahectane': 115, 	
        'hexadecahectane': 116, 	
        'heptadecahectane': 117, 	
        'octadecahectane': 118, 	
        'nonadecahectane': 119, 	
        'icosahectane': 120
    }
    alkane = alkane.casefold()
    try:
        carbons = carbon_dict[alkane]
        return carbons
    except KeyError:
        print('The n-alkane molecule ' + alkane + ' is not recognized.')
        raise   

def carbons_to_alkane(carbons):
    """Gives the name of the n-alkane molecule with a given number of
    carbon atoms.

    Parameters
    ----------
    carbons : int
        Number of carbons in the n-alkane.

    Returns
    -------
    alkane : string
        Name of the n-alkane molecule with `carbons` carbon atoms.
   
    """

    molecule_dict = {
        1: 'methane',
        2: 'ethane',
        3: 'propane',
        4: 'butane',
        5: 'pentane',
        6: 'hexane',
        7: 'heptane',
        8: 'octane',
        9: 'nonane',
        10: 'decane',
        11: 'undecane',
        12: 'dodecane',
        13: 'tridecane',
        14: 'tetradecane',
        15: 'pentadecane',
        16: 'hexadecan',
        17: 'heptadecane',
        18: 'octadecane',
        19: 'nonadecane',
        20: 'icosane',
        21: 'henicosane',
        22: 'docosane',
        23: 'tricosane',
        24: 'tetracosane',
        25: 'pentacosane',
        26: 'hexacosane',
        27: 'heptacosane',
        28: 'octacosane',
        29: 'nonacosane',
        30: 'triacontane',
        31: 'hentriacontane',
        32: 'dotriacontane',
        33: 'tritriacontane',
        34: 'tetratriacontane',
        35: 'pentatriacontane',
        36: 'hexatriacontane',
        37: 'heptatriacontane',
        38: 'octatriacontane',
        39: 'nonatriacontane',
        40: 'tetracontane',
        41: 'hentetracontane',
        42: 'dotetracontane',
        43: 'tritetracontane',
        44: 'tetratetracontane',
        45: 'pentatetracontane',
        46: 'hexatetracontane',
        47: 'heptatetracontane',
        48: 'octatetracontane',
        49: 'nonatetracontane',
        50: 'pentacontane',
        51: 'henpentacontane',
        52: 'dopentacontane',
        53: 'tripentacontane',
        54: 'tetrapentacontane',
        55: 'pentapentacontane',
        56: 'hexapentacontane',
        57: 'heptapentacontane',
        58: 'octapentacontane',
        59: 'nonapentacontane',
        60: 'hexacontane', 	
        61: 'henhexacontane', 	
        62: 'dohexacontane', 	
        63: 'trihexacontane', 	
        64: 'tetrahexacontane', 	
        65: 'pentahexacontane', 	
        66: 'hexahexacontane', 	
        67: 'heptahexacontane', 	
        68: 'octahexacontane', 	
        69: 'nonahexacontane', 	
        70: 'heptacontane', 	
        71: 'henheptacontane', 	
        72: 'doheptacontane', 	
        73: 'triheptacontane', 	
        74: 'tetraheptacontane', 	
        75: 'pentaheptacontane', 	
        76: 'hexaheptacontane', 	
        77: 'heptaheptacontane', 	
        78: 'octaheptacontane', 	
        79: 'nonaheptacontane', 	
        80: 'octacontane', 	
        81: 'henoctacontane', 	
        82: 'dooctacontane', 	
        83: 'trioctaane', 	
        84: 'tetraoctacontane', 	
        85: 'pentaoctacontane', 	
        86: 'hexaoctacontane', 	
        87: 'heptaoctacontane', 	
        88: 'octaoctacontane', 	
        89: 'nonaoctacontane', 	
        90: 'nonacontane', 	
        91: 'hennonacontane', 	
        92: 'dononacontane', 	
        93: 'trinonacontane', 	
        94: 'tetranonacontane', 	
        95: 'pentanonacontane', 	
        96: 'hexanonacontane', 	
        97: 'heptanonacontane', 	
        98: 'octanonacontane', 	
        99: 'nonanonacontane', 	
        100: 'hectane', 	
        101: 'henihectane', 	
        102: 'dohectane', 	
        103: 'trihectane', 	
        104: 'tetrahectane', 	
        105: 'pentahectane', 	
        106: 'hexahectane', 	
        107: 'heptahectane', 	
        108: 'octahectane', 	
        109: 'nonahectane', 	
        110: 'decahectane', 	
        111: 'undecahectane', 	
        112: 'dodecahectane', 	
        113: 'tridecahectane', 	
        114: 'tetradecahectane', 	
        115: 'pentadecahectane', 	
        116: 'hexadecahectane', 	
        117: 'heptadecahectane', 	
        118: 'octadecahectane', 	
        119: 'nonadecahectane', 	
        120: 'icosahectane'
    }	
    try:
        molecule = molecule_dict[carbons]
        return molecule
    except KeyError:
        print('The name of the n-alkane molecule with ' + str(carbons) +
            ' carbon atoms is unknown. Must be an integer between 1 and 120.')
        raise

