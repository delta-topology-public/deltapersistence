"""Utilities for handling GUDHI and DELTA data types.

Converts internal data types used by GUDHI and provides methods for
reading and writing different data formats used by the DELTA team. In
particular, the following standard formats are used:

=========================== ================    ================    =============
Semantic Type               Python data type    File Format         Specification
=========================== ================    ================    =============
Energy landscape (sampled)  Numpy array         Plaintext (`.dat`)  (Tab-separated values)
Energy landscape (meshed)   Numpy ndarray       Numpy (`.npy`)      https://numpy.org/doc/stable/reference/generated/numpy.lib.format.html#module-numpy.lib.format
Persistence intervals       List of tuples      GUDHI (`.pers`)     https://gudhi.inria.fr/python/latest/fileformats.html#persistence-diagram
=========================== ================    ================    =============

    Copyright (C) 2020 Joshua Mirth and Johnathan Bush

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


Functions
---------

* intervals_in_dimension
* plot_persistence_barcode
* plot_persistence_diagram
* remove_inf_bars
* sequential_maxmin
* write_intervals

"""

# Standard packages
import numpy as np
import random
from pathlib import Path 
from os import path

def write_intervals(intervals, filename, message='Persistence Intervals'):
    """Write the output persistence intervals to file.

    Takes a list of persistence intervals and writes them to a file in
    the persistence format specified by GUDHI. These can then be read
    and plotted using GUDHI's built-in methods.

    Parameters
    ----------
    filename : string
        Name of file to write intervals to. Should end with ".pers",
        though this is not enforced.
    intervals : list of tuples
        List of persistence intervals in the form (dim,(birth,death)).
    message : string, optional
        Message to print as the first line of the file.

    """

    formatted_intervals = ''
    for i in intervals:
        formatted_intervals = formatted_intervals + str(i) + '\n'
    formatted_intervals = formatted_intervals.replace(')','')
    formatted_intervals = formatted_intervals.replace('(','') 
    formatted_intervals = formatted_intervals.replace(',','')
    with open(filename, 'w') as filehandle:
        filehandle.writelines('# ' + message + '\n')
        filehandle.writelines(item for item in formatted_intervals)

def remove_inf_bars(persistence):
    """Remove any bars in the persistence which are born at infinity.

    When using the periodic cubical complex method it is possible for
    persistence intervals to be born at infinity. These give an error if
    passed to barcode or diagram plotting methods and need to be
    removed.

    Parameters
    ----------
    persistence : list of tuples
        A list of tuples of the form (dimn, (birth, death)).

    Returns
    -------
    intervals : list of persistence
        The input list, but with any entries where birth=infinity
        removed.

    """
    # Assumes the persistence is sorted by dimension from largest to
    # smallest (as it should be from all standard methods).
    intervals = persistence.copy()  # Do not treat the input as mutable.
    max_dim = intervals[0][0]
    for i in range (0,max_dim+1):
        try:
            intervals.remove((i,(np.inf,np.inf)))
            # TODO: probably better to pass this message as a return
            # value.
            print('Removed interval born at infinity in dimension ' +
                str(i))
        except ValueError:
            pass
    return intervals

def intervals_in_dimension(persistence,dimn):
    """Returns the persistence intervals in the given dimension.

    When GUDHI computes persistence it always computes in all
    dimensions, however sometimes it is desirable to only return the
    intervals in a single dimension (e.g. if there are a very large
    number of total intervals).

    Parameters
    ----------
    persistence : list of tuples
        A list of tuples of the form (dimn, (birth, death)).
    dimn : int
        Dimension of homology to return.

    Returns
    -------
    intervals : list of tuples
        The list of intervals in the form (dimn, (birth, death)) where
        dimn is restricted to the specified dimension.

    Notes
    -----
    This function is not strictly necessary since most GUDHI objects
    possessing persistence have a `persistence_intevals_in_dimension()`
    method. However, sometimes it is convenient to work with the
    already-returned list of intervals rather than the complex object.
    
    """

    intervals = []
    for i in persistence:
        if i[0] == dimn:
            intervals.append(i)
    return intervals

def sequential_maxmin(coords,subsample_size,seed=0):
    """Use a sequential maxmin algorithm to downsample input data.

    Parameters
    ----------
    coords : numpy float array
        Array of coordinates in d-dimensional space.
    subsample_size : integer
        Size of desired downsample.
    seed : integer, optional
        Index of initial point. Default is to use the first point in
        the data. Users may desire to choose this point randomly.

    Returns
    -------
    maxmin_indices : integer list
        List containing indices of the coordinates selected by maxmin.

    """

    maxmin_indices = [seed]
    distances = np.full(len(coords),np.inf)
    while len(maxmin_indices) < subsample_size:
        pt = coords[seed]
        distances = np.minimum(np.sum((coords - pt)**2,axis=1), distances)
        seed = distances.argmax()
        maxmin_indices.append(seed)
    return maxmin_indices

